<!-- Back To Top -->
        <a href="void(0);" class="js-back-to-top back-to-top">Top</a>
//
//        <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
//        <!-- CORE PLUGINS -->
        <script src="vendor/jquery.min.js" type="text/javascript"></script>
        <script src="vendor/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

//        <!-- PAGE LEVEL PLUGINS -->
        <script src="vendor/jquery.easing.js" type="text/javascript"></script>
        <script src="vendor/jquery.back-to-top.js" type="text/javascript"></script>
        <script src="vendor/jquery.smooth-scroll.js" type="text/javascript"></script>
        <script src="vendor/jquery.wow.min.js" type="text/javascript"></script>
        <script src="vendor/swiper/js/swiper.jquery.min.js" type="text/javascript"></script>
        <script src="vendor/masonry/jquery.masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="vendor/masonry/imagesloaded.pkgd.min.js" type="text/javascript"></script>

//        <!-- PAGE LEVEL SCRIPTS -->
        <script src="js/layout.min.js" type="text/javascript"></script>
        <script src="js/components/wow.min.js" type="text/javascript"></script>
        <script src="js/components/swiper.min.js" type="text/javascript"></script>
        <script src="js/components/masonry.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                <?php
                        foreach ($resultMenuJs as $menu) {
                ?>
                        $("#<?php echo $menu['HrefMenu'] ;?>").click(function() {
                            $.ajax({
                                url: 'include/content/<?php echo $menu['HrefMenu']; ?>.php',
                                dataType: 'html',
                                success: function (data) {
                                    console.log(data);
                                    $('section[id=content]').fadeOut("slow");
                                    $('section[id=content]').html(" ");
                                    $('section[id=content]').html(data);
                                    $('section[id=content]').fadeIn("slow");
                                    $('li.nav-item>a').removeClass("active");
                                    $("#<?php echo $menu['HrefMenu'] ;?>>a").addClass("active");
                                }
                            });
                        });
                <?php
                        }
                ?>
                        });
        </script>