        <!--========== FOOTER ==========-->
        <footer class="footer">
            <!-- Copyright -->
            <div class="content container">
                <div class="row">
                    <div class="col-xs-6">
                        <img class="footer-logo" src="img/Logo_Roger_Benaim.png" alt="Roger Benaim logo">
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="margin-b-0"><a class="color-base fweight-700"> Transport Internationaux Roger Benaim <br> 40 Rue du Professeur Gosset <br> 75018 Paris <br> 01-46-27-48-48 </p>
                    </div>
                </div>
                <!--// end row -->
            </div>
            <!-- End Copyright -->
        </footer>