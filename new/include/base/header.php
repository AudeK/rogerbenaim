<?php
session_start();
?>
        <!--========== HEADER ==========-->
        <header class="header navbar-fixed-top">
            <!-- Navbar -->
            <nav class="navbar" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>

                        <!-- Logo -->
                        <div class="logo">
                            <a class="logo-wrap" href="index.html">
                                <img class="logo-img logo-img-main" src="img/Logo_Roger_Benaim.png" alt="Roger Benaim logo ">
                                <img class="logo-img logo-img-active" src="img/Logo_Roger_Benaim.png" alt="Roger Benaim logo">
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container">
                            <ul class="navbar-nav navbar-nav-right">
                                <?php 
                                        //On parcourt chaque ligne du tableau avec une boucle
                                $i=0;
                                        foreach ($resultMenu as $menu) {
                                            if(isset($_SESSION['pages']) && $_SESSION['pages'] == $menu['HrefMenu']){
                                                $class = "active";
                                            }elseif ($i == 0  && !isset($_SESSION['pages'])) {
                                                $class = "active";
                                            }else{
                                                $class = " ";
                                            }
                                    ?>
                                    <li class="nav-item" id="<?php echo $menu['HrefMenu'];  ?>">
                                        <a class="nav-item-child nav-item-hover <?php echo $class; ?>" >
                                        <?php 
                                            //On affiche l'element du menu 
                                            echo ucfirst($menu['LibMenu']);
                                        ?>
                                        </a>
                                    </li>
                                    <?php
                                    $i++;
                                        }
                                    ?>
                                
                                <!--<li class="nav-item">
                                    <a class="nav-item-child nav-item-hover" >Présentation</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-item-child nav-item-hover" >Nos services</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-item-child nav-item-hover" >Utilitaire</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-item-child nav-item-hover" >Contact</a>
                                </li>-->
                            </ul>
                        </div>
                    </div>
                    <!-- End Navbar Collapse -->
                </div>
            </nav>
            <!-- Navbar -->
        </header>