<?php
session_start();
//On definit les données neccesaire a la connexion de la db
$host = 'localhost';
$user = 'root';
$pass = 'root';
$db_name = 'RBenaim';
//On se connecte a la db
$db = new PDO('mysql:host=' . $host . ';dbname=' . $db_name, $user, $pass);
//On fait une requete a la db + Enregistre resultat dans $result 
$resultMenu = $db->query("SELECT LibMenu,HrefMenu FROM MENU ORDER BY IdMenu ASC");
$resultMenuJs = $db->query("SELECT LibMenu,HrefMenu FROM MENU ORDER BY IdMenu ASC");
?>

<!DOCTYPE html>
<html>
<head>
    <?php
    include("include/base/source.css.php");
    ?>
</head>
<body>

    <?php
    include('include/base/header.php');
    ?>
<section id="content">
    <?php
    if(isset($_SESSION['pages'])){
        include('include/content/'.$_SESSION['pages'].'.php');
    }else{
    include('include/content/home.php');
    }
    ?>
</section>
        <?php
        include('include/base/footer.php');
        include('include/base/source.js.php');
        ?>



</body>
</html>	
