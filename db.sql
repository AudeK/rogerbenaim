-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  jeu. 18 jan. 2018 à 16:18
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `RBenaim`
--

-- --------------------------------------------------------

--
-- Structure de la table `Menu`
--

CREATE TABLE `Menu` (
  `IdMenu` int(11) NOT NULL,
  `LibMenu` varchar(100) NOT NULL,
  `HrefMenu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Menu`
--

INSERT INTO `Menu` (`IdMenu`, `LibMenu`, `HrefMenu`) VALUES
(1, 'accueil', 'home'),
(2, 'presentation', 'presentation'),
(3, 'nos services', 'services'),
(4, 'utilitaire', 'utilitaire'),
(5, 'contact', 'contact');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Menu`
--
ALTER TABLE `Menu`
  ADD PRIMARY KEY (`IdMenu`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Menu`
--
ALTER TABLE `Menu`
  MODIFY `IdMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
